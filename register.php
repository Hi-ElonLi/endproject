<?php


require_once('connection.php');
session_start();

if (isset($_POST['btn_register'])) {
    $username = $_POST['txt_username'];
    $email = $_POST['txt_email'];
    $password = $_POST['txt_password'];
    $role = "user";
    $name = $_POST['txt_name'];
    $Tel = $_POST['txt_tel'];

    // $image_file_car = $_FILES['txt_file_car']['name'];

    $imgg = $_FILES["iimg"]["name"];

    $imgg_temp = $_FILES["iimg"]["tmp_name"];

     move_uploaded_file($imgg_temp, 'upload_person/' . $imgg);


    if (empty($username)) {
        $errorMsg = "Please enter username";
    }
    else if (empty($email)) {
        $errorMsg = "Please enter email";
    } 
    else if (empty($password)) {
        $errorMsg = "Please enter password";
    } else if (strlen($password) < 5) {
        $errorMsg = "Password must be atleast 6 characters";
    } else {
        try {
            //  $select_stmt = $db->prepare("SELECT username, email FROM usertbls WHERE username = :uname OR email = :uemail");
            //   ***sql เทียบ ไอดี ซ้ำ
            $select_stmt = $db->prepare("SELECT u_name FROM usertbls WHERE u_name = :uname ");
            // ****จะดึงต้องประกาศ
            $select_stmt->bindParam(":uname", $username);


            // $select_stmt->bindParam(":uemail", $email);
            $select_stmt->execute();
            $row = $select_stmt->fetch(PDO::FETCH_ASSOC);

            //  *************   กัน ไอดีซ้ำ กัน

            if ($row['u_name'] == $username) {
                $errorMsg = "Sorry username already exists";
            }
            else if ($row['email'] == $email) {
                $errorMsg = "Sorry email already exists";
            } 
            else if (!isset($errorMsg)) {
                $insert_stmt = $db->prepare("INSERT INTO usertbls(email,u_name, u_pass, role, name , tel ,img) 
                                                            VALUES (:uemail, :uname, :upass, :urole, :u_name2, :utel, :uimg)");
                $insert_stmt->bindParam(":uname", $username);
                $insert_stmt->bindParam(":uemail", $email);
                $insert_stmt->bindParam(":upass", $password);
                $insert_stmt->bindParam(":urole", $role);
                $insert_stmt->bindParam(":u_name2", $name);
                $insert_stmt->bindParam(":utel", $Tel);
                $insert_stmt->bindParam(":uimg", $imgg);

                if ($insert_stmt->execute()) {
                    $_SESSION['success'] = "Register Successfully...";
                    header("location: index.php");
                }
            }
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Page</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        <h1 class="mt-3">Register Form</h1>
        <hr>
    

        <?php if (isset($errorMsg)) : ?>
            <div class="alert alert-danger">
                <h3>
                    <?php
                    echo $errorMsg;

                    ?>
                </h3>
            </div>
        <?php endif ?>
        <form action="register.php" method="post" enctype="multipart/form-data">
            <!-- <form action="register.php" method="post" class="form-horizontal my-5"> -->

            <div class="form-group">

                <!-- *****ส่งค่า for'username' ไปเทียบ -->
                <!-- <label for="user" class="col-sm-3 control-label">Username</label> -->
                <h3>Username</h3>
                <div class="col-sm-12">
                    <input type="text" name="txt_username" class="form-control" required placeholder="Enter username">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-12">
                    <input type="text" name="txt_email" class="form-control" required placeholder="Enter email">
                </div>
            </div>


            <div class="col-sm-12">
                <label for="password" class="col-sm-3 control-label">Password</label>
                <input type="password" name="txt_password" class="form-control" required placeholder="Enter password">
            </div>

            <div class="col-sm-12">
                <label class="col-sm-3 control-label">ชื่อ-นามสกุล</label>
                <input type="text" name="txt_name" class="form-control" required placeholder="Enter ">
            </div>
            <div class="col-sm-12">
                <label class="col-sm-3 control-label">โทรศัพท์</label>

                <input type="text" name="txt_tel" class="form-control" required placeholder="Enter">
            </div>
            <div class="col-sm-12">
                <label class="col-sm-3 control-label">อัพภาพหน้าตรง</label>
                <!-- <input type="file" name="txt_file_car" class="my-input"> -->
                <!-- <input type="file" name="txt_img" class="form-control" required placeholder="Enter "> -->
                <input type="file" name="iimg" class="form-control" required placeholder="Enter ">

            </div>


            <br><br><br>

            <div class="form-group">
                <div class="">
                    <input type="submit" name="btn_register" class="btn btn-primary" style="width: 100%;" value="ลงทะเบียน">
                </div>
            </div>

            <div class="form-group text-center">
                <div class="col-sm-12 mt-3">
                    Already have an account ?
                    <p><a href="index.php">เข้าสู่ระบบ</a></p>
                </div>
            </div>

        </form>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>