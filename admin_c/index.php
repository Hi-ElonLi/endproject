<?php

session_start();

require_once "../connection.php";

if (!isset($_SESSION['admin_login'])) {
    header("location: ../index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Page</title>
    <link rel="stylesheet" href="css/adminstyle.css">
    <!-- carrental -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/css/bootstrap.min3.css" type="text/css"> -->
    <!--Custome Style -->
    <!-- <link rel="stylesheet" href="../resources/css/style2.css" type="text/css"> -->
    <!-- <link rel="stylesheet" href="../resources/css/bootstrap.min2.css" type="text/css"> -->
    <!--Custome Style -->
    <!-- <link rel="stylesheet" href="../resources/css/style2.css" type="text/css"> -->
    <!-- //**** flex */ -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- grid -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>

    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>
            <div class="showinfo">
                <h1>Welcome to Admin Panel</h1>

                <img width="640" height="360" src="img/admin.jpg" alt="">


                <div class="row">
                    <div class="col-md-12">
                        <!-- //** แถว1 */ -->
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <div style="border:solid steelblue ">
                                    <div class="bg-primary text-light ">
                                        <?php
                                        $sql4 = "SELECT id from usertbls ";
                                        $query4 = $db->prepare($sql4);
                                        $query4->execute();
                                        $results4 = $query4->fetchAll(PDO::FETCH_OBJ);
                                        $subscribers = $query4->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($subscribers)-1; ?></div>
                                        <div class="stat-panel-title text-uppercase">
                                            <h3>ผู้ที่ลงทะเบียนทั้งหมด</h3>
                                        </div>
                                    </div>
                                    <a href="person_app.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div style="border:solid #6495ED ">
                                    <div style="background-color:#6495ED;" class=" text-light ">
                                        <?php
                                        $sql3 = "SELECT id from user_applicate ";
                                        $query3 = $db->prepare($sql3);
                                        $query3->execute();
                                        $results3 = $query3->fetchAll(PDO::FETCH_OBJ);
                                        $car = $query3->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($car); ?></div>
                                        <div class="stat-panel-title text-uppercase">
                                            <h3>จำนวนรถทั้งหมด</h3>
                                        </div>
                                    </div>
                                    <a href="car_app.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div style="border:solid #00CED1 ">
                                    <div style="background-color:#00CED1;" class=" text-light ">
                                    <?php
                                        $sql2 = "SELECT id from user_applicate where status is NULL ";
                                        $query2 = $db->prepare($sql2);
                                        $query2->execute();
                                        $results2 = $query2->fetchAll(PDO::FETCH_OBJ);
                                        $new_app = $query2->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($new_app); ?></div>
                                        <div class="stat-panel-title text-uppercase">
                                            <h3>คำร้องสมัครงาน มาใหม่</h3>
                                        </div>
                                    </div>
                                    <a href="new_app.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- //** แถว2 */ -->
                        <div style="margin-top: 20px" class="row">
                            <div class="col-md-4 text-center">
                                <div style="border:solid #8B0000 ">
                                    <div style="background-color :#8B0000" class="text-light ">
                                        <?php
                                        $sql1 = "SELECT id from user_applicate  ";
                                        $query1 = $db->prepare($sql1);
                                        $query1->execute();
                                        $results1 = $query1->fetchAll(PDO::FETCH_OBJ);
                                        $all_app = $query1->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($all_app); ?></div>
                                        <div class="stat-panel-title text-uppercase"><h3>คำร้องสมัครงาน ทั้งหมด</h3></div>
                                    </div>
                                    <a href="all_app.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div style="border:solid #D2691E ">
                                    <div style="background-color :#D2691E" class="text-light ">
                                        <?php
                                        $wait='รอ';
                                        $sql = "SELECT id from user_book where b_status=:wait   "; //***แก้ ส่งตัวแปร */
                                        $query = $db->prepare($sql);
                                        $query->bindParam(':wait',$wait);
                                        $query->execute();
                                        $results = $query->fetchAll(PDO::FETCH_OBJ);
                                        $new_book = $query->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($new_book); ?></div>
                                        <div class="stat-panel-title text-uppercase"><h3>คำร้องจองรถ ใหม่</h3></div>
                                    </div>
                                    <a href="new_book.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div style="border:solid #B8860B ">
                                    <div style="background-color :#B8860B" class="text-light ">
                                        <?php
                                        $sqlh = "SELECT id from user_book   "; //***แก้ ส่งตัวแปร */
                                        $queryh = $db->prepare($sqlh);
                                        $queryh->execute();
                                        $resultsh = $queryh->fetchAll(PDO::FETCH_OBJ);
                                        $all_book = $queryh->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($all_book); ?></div>
                                        <div class="stat-panel-title text-uppercase"><h3>คำร้องจองรถ ทั้งหมด</h3></div>
                                    </div>
                                    <a href="all_book.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

            <!-- //************ -->

            <!--          <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body bk-primary text-light">
                                    <div class="stat-panel text-center">
                                        <?php
                                        $sql3 = "SELECT id from user_applicate ";
                                        $query3 = $db->prepare($sql3);
                                        $query3->execute();
                                        $results3 = $query3->fetchAll(PDO::FETCH_OBJ);
                                        $car = $query3->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($car); ?></div>
                                        <div class="stat-panel-title text-uppercase">จำนวนรถทั้งหมด</div>
                                    </div>
                                </div>
                                <a href="manage-subscribers.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body bk-primary text-light">
                                    <div class="stat-panel text-center">
                                        <?php
                                        $sql2 = "SELECT id from user_applicate where status is NULL ";
                                        $query2 = $dbh->prepare($sql2);
                                        $query2->execute();
                                        $results2 = $query2->fetchAll(PDO::FETCH_OBJ);
                                        $new_app = $query2->rowCount();
                                        ?>
                                        <div class="stat-panel-number h1 "><?php echo htmlentities($new_app); ?></div>
                                        <div class="stat-panel-title text-uppercase">คำร้องสมัครงาน มาใหม่ </div>
                                    </div>
                                </div>
                                <a href="manage-subscribers.php" class="block-anchor panel-footer">ไปดู <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
    </section>

</body>

</html>

<!-- <?php  ?> -->