<?php

session_start();
// include('')
require_once "../connection.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>
    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>

            <div class="showinfo">
                <h1 style=" background-color:  #ecfd00!important;">ผู้ที่กำลังใช้บริการรถ</h1>
                <?php
                $bid = $_GET['b_id'];
                $hire = $_REQUEST['hire'];
                echo $hire;
                echo $bid;

                $sql2 = "SELECT  id FROM  user_book   where b_hire_name=:hire  ";
                // $sql = "SELECT * FROM user_applicate   where user_applicate.hire_name=:hire  ";


                $q = $db->prepare($sql2);
                $q->bindParam(':hire', $hire, PDO::PARAM_STR);
                $q->execute();
                $re = $q->fetch(PDO::FETCH_ASSOC);

                $sql = "SELECT  usertbls.* FROM  usertbls   where usertbls.u_name=:hire  ";
                // $sql = "SELECT * FROM user_applicate   where user_applicate.hire_name=:hire  ";


                $query = $db->prepare($sql);
                $query->bindParam(':hire', $hire, PDO::PARAM_STR);
                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);


                $cnt = 1;

                $cnt = $query->rowCount();
                // ** */ แสดงหลายรายการ .sizeof()  ไปหามา

                if ($query->rowCount() > 0) {
                    foreach ($results as $row) {

                        $us_name = $row->name;
                        $us_img = $row->img;
                        $us_uname = $row->u_name;
                        $us_tel = $row->tel;
                        $us_email = $row->email;
                        $us_reg = $row->u_Regdate;


                ?>
                <div>

                    <table class="table " style="font-size: 20px;border:double;">

                        <th>
                            <img style="width:200px ;" src="../upload_person/<?php echo $row->img; ?>">
                        </th>

                        <td>

                            <table>


                                <tr>
                                    <th>username</th>
                                    <td>
                                        <?php echo htmlentities($us_uname); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ชื่อ นามสกุล</th>
                                    <td>
                                        <?php echo htmlentities($us_name); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>เบอร์โทร</th>
                                    <td>
                                        <?php echo htmlentities($us_tel); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>อีเมลล์</th>
                                    <td>
                                        <?php echo htmlentities($us_email); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>วันที่ทำรายการ</th>

                                    <td>
                                        <span class="badge badge-primary">
                                            <?php echo htmlentities($us_reg); ?>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div style="margin-top: 50px;" class="form-group">
                                <span class="">
                                    <a style="color: white;background-color:blue" href="book_detail.php?bid=<?php echo  $re['id']  ?>">ดูรายละเอียดจองรถ</a>
                                </span>
                            </div>
                            <br>
                            <hr style="border: solid;"><br>
                            <!-- <div>
                                <input style="background-color: green; color:white" type="submit" name="btn" value="อนุมัติส่งเสร็จสิ้น" onclick="return confirm('คุณแน่ใจเหรอว่า จะอนุมัติ!!');">
                            </div> -->

                        </td>

                    </table>

                </div>

                <?php 
                    }
                }  ?>



            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>

