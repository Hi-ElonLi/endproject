<?php

session_start();
// include('')
require_once "../connection.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">
    <link rel="stylesheet" id="css-main" href="./assets/css/codebase.min.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>

    <section class="content">
        <div class="content__grid">
        <?php include('nav.php');?>
            <div class="showinfo">
                <h1 style=" background-color:  #ecfd00!important;">ดูคำร้องการสมัคร ทั้งหมดมาใหม่</h1>

                <table class="table table-striped table-bordered table-hover" style="font-size: 30px;">

                    <tr>
                        <th>No.</th>
                        
                        <th>BookingID</th>
                        <th>วันที่ทำรายการ</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <!-- ***********    หัวตาราง ***********    หัวตาราง ***********    หัวตาราง ***********    หัวตาราง -->
                    <?php

                    // **********แก้โชว์ status != "" ของ  usertbls ด้วย************+ admin status == 555
                    // อย่าเอา  usertbl มาอ้าง ดึงชื่อ เพราะจะเกี่ยวกับ status จเปลี่ยตามไปด้วย

                    // $sql = "SELECT * FROM user_applicate join usertbls on user_applicate.id=usertbls.id  WHERE status is NULL";

                    /// เงื่อนไขใหม่
                    $sql = "SELECT * FROM user_applicate  WHERE status is NULL  ";




                    $query = $db->prepare($sql);
                    $query->execute();
                    $results = $query->fetchAll(PDO::FETCH_OBJ);


                    $cnt = 1;


                    if ($query->rowCount() > 0) {
                        foreach ($results as $row) {               ?>

                            <!-- // ************** tr ***********// ************** tr ***********// ************** tr *********** -->
                            <tr>
                                <td class="text-center"><?php echo htmlentities($cnt); ?></td>


                                <!-- *  id จาก user_applicate -->
                                <td> <?php echo htmlentities($row->BookingID); ?> </td>


                                <td>
                                    <span class="badge badge-primary"><?php echo htmlentities($row->Regdate); ?></span>
                                </td>
                                <!-- <td class="font-w600"><?php echo htmlentities($row->u_name); ?></td> -->

                                <?php if ($row->status == "") { ?>
                                    <!-- //******************td -->
                                    <td class="font-w600"><?php echo "รอ...อนุมัติ"; ?></td>
                                <?php } else { ?>
                                    <!-- //******************td -->
                                    <td class="d-none d-sm-table-cell">
                                        <!-- โชว์อนุมัติ กับ ไม่อนุมัติ -->
                                        <span class="badge badge-primary"><?php echo htmlentities($row->status); ?></span>

                                    </td>
                                <?php } ?>
                                <!-- / //**************************************************editid ส่ง $_GET['editid'];ต่อหน้า book detail******************td -->
                                <td class="d-none d-sm-table-cell"><a href="app-detail.php?editid=<?php echo htmlentities($row->id); ?>">จัดการ</a></td>

                            </tr>

                    <?php $cnt = $cnt + 1;
                        }
                    } ?>




                </table>

            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>