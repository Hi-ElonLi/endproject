<?php
session_start();
require_once "../connection.php";
// ***วางไว้นี้ เดะมันไม่อ่าน
$bid = $_GET['bid'];


if (strlen($_SESSION['admin_login']) == 0) {
	header('location:../index.php');
} else {
	if (isset($_REQUEST['eid'])) {

		$eid = intval($_GET['eid']);

		// $sq = "SELECT bID from user_book	where id=:eid ";
		// $select = $db->prepare($sq);
		// $select->bindParam(':eid', $eid);
		// $select->execute();
		// $row = $select->fetch(PDO::FETCH_ASSOC);
		// unlink("../upload_bill/" .$row['b_img_bill']); // unlin functoin permanently remove your file

		$status = 'ไม่อนุมัติ';
		$sql = "UPDATE  user_book SET b_status=:status WHERE  id=:eid";

		$query = $db->prepare($sql);
		$query->bindParam(':status', $status, PDO::PARAM_STR);
		$query->bindParam(':eid', $eid, PDO::PARAM_STR);

		try {
			$query->execute();
			echo "<script>alert('ทำการ..ไม่อนุมัติ..เรียบร้อยแล้ว...');</script>";
			// echo "<script type='text/javascript'> document.location = 'all_book.php; </script>";
			header('refresh:0.2;all_book.php');
		} catch (PDOException $e) {
			$e->getMessage();
		}
	}


	if (isset($_REQUEST['aeid'])) {
		$aeid = intval($_GET['aeid']);
		$status = 'อนุมัติแล้ว';
		// $busy = 'ไม่ว่าง';
		$hire = $_SESSION['bh'];

		// ******* อัปเดต  $busy **********
		// $sq = "SELECT * from user_book	where id=:eid ";
		// $select = $db->prepare($sq);
		// $select->bindParam(':eid', $eid);
		// $select->execute();
		// $results = $select->fetchAll(PDO::FETCH_OBJ);
		//  ***************อัปเดต  $busy ใน app************


		// if ($select->rowCount() > 0) {
		// 	foreach ($results as $row) {
//** UPDATE   user_applicate SET busy=:busy */

		// $usq = "UPDATE   user_applicate SET busy=:busy ,hire_name=:hire,b_ID=:bid WHERE  id=:app_id";
		// $update = $db->prepare($usq);
		// $update->bindParam(':busy', $busy, PDO::PARAM_STR);
		// $update->bindParam(':hire', $hire, PDO::PARAM_STR);
		// $update->bindParam(':bid', $_SESSION['bid'], PDO::PARAM_STR);
		// $update->bindParam(':app_id', $_SESSION['b'], PDO::PARAM_STR);   // select from user_book
		// $update->execute();
		// 	}
		// }

	

		$sql = "UPDATE user_book SET b_status = :status WHERE  id=:aeid";
		$query = $db->prepare($sql);
		$query->bindParam(':status', $status, PDO::PARAM_STR);
		$query->bindParam(':aeid', $aeid, PDO::PARAM_STR);
		$query->execute();
		echo "<script>alert('อนุมัติการจองรถ..สำเร็จแล้ว...');</script>";
		echo "<script type='text/javascript'> document.location = 'all_book.php'; </script>";
	}


?>

	<!doctype html>
	<html lang="en" class="no-js">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="theme-color" content="#3e454c">

		<title>รายละเอียดหนังสือจองรถ | New Bookings </title>

		<link rel="stylesheet" href="css/adminstyle.css">
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<style>
			.errorWrap {
				padding: 10px;
				margin: 0 0 20px 0;
				background: #fff;
				border-left: 4px solid #dd3d36;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
				box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
			}

			.succWrap {
				padding: 10px;
				margin: 0 0 20px 0;
				background: #fff;
				border-left: 4px solid #5cb85c;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
				box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
			}
		</style>

	</head>

	<body>
		<header>
			<div class="container">
				<h1>Welcome to Admin Page </h1>
			</div>
		</header>
		<section class="content">
			<div class="content__grid">
				<?php include('nav.php'); ?>
				<!-- ****** id="main" -->
				<div id="main" class="ts-main-content">

					<div class="content-wrapper">
						<div class="container-fluid">

							<div class="row">
								<div class="col-md-12">

									<h2 class="page-title">รายละเอียด หนังสือจองรถ</h2>

									<!-- Zero Configuration Table -->
									<div class="panel panel-default">

										<div class="panel-body">


											<div id="print">
												<table border="1" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">

													<tbody>

														<?php
														// $bid = intval($_REQUEST['bid']);



														// *******************************
														$sql = "SELECT * FROM user_book WHERE id=:bid ";

														$query = $db->prepare($sql);
														$query->bindParam(':bid', $bid);
														$query->execute();
														$results = $query->fetchAll(PDO::FETCH_OBJ);
														$cnt = 1;
														if ($query->rowCount() > 0) {
															foreach ($results as $result) {				?>


																<?php $_SESSION['bid']=$result->id ?>

																<h5><?php echo 	htmlentities($_SESSION['b'] = $result->bID); ?></h5>

																<h3 style="text-align:center; color:red">รายละเอียดของหนังสือที่#<?php echo htmlentities($result->b_BookingID); ?> </h3>

																<tr>
																	<th colspan="4" style="text-align:center;color:blue">ข้อมูลผู้จองรถ</th>
																</tr>
																<tr>
																	<th>เลขหนังสือ</th>
																	<td>#<?php echo htmlentities($result->b_BookingID); ?></td>
																	<th>ชื่อ-นามสกุล</th>
																	<td><?php echo htmlentities($_SESSION['bh'] = $result->b_hire_name); ?></td>
																</tr>
																<tr>
																	<th>อีเมลล์ </th>
																	<td>
																		<?php echo htmlentities($result->b_email); ?>
																	</td>
																	<th>เบอร์โทร</th>
																	<td><?php echo htmlentities($result->b_tel); ?></td>
																</tr>



																<tr>
																	<th colspan="4" style="text-align:center;color:blue">รายละเอียด หนังสือจอง</th>
																</tr>
																<tr>
																	<th>ชื่อเจ้าของรถ</th>
																	<td>
																		<a href="app-detail.php?editid=<?php echo htmlentities($result->bID); ?>"><?php echo htmlentities($result->b_driver_name); ?></a>

																	</td>
																	<th>ข้อมูลรถ</th>
																	<td>
																		<a href="app-detail.php?editid=<?php echo htmlentities($result->bID); ?>"><?php echo htmlentities($result->b_car_type); ?></a>
																	</td>

																</tr>

																<tr>
																	<th>วันทำการ</th>
																	<td><?php echo htmlentities($result->FromDate); ?></td>
																	<th></th>
																	<td></td>
																</tr>

																<tr>
																	<th>จุดรับ</th>
																	<td><?php echo htmlentities($result->b_s_distance); ?></td>
																	<th>จุดส่ง</th>
																	<td><?php echo htmlentities($result->b_e_distance); ?></td>
																</tr>

																<tr>
																	<th>ระยะทาง</th>
																	<td><?php echo htmlentities($tdays = $result->b_distance); ?></td>
																	<th>ราคา:KM - ราคาตามประเภทรถ</th>
																	<td>
																		<?php

																		$m = $result->b_distance;

																		if ($result->b_car_type == 'จักรยานยนต์') {
																			if ($m <= 30) {
																				$price1 = 6.5;
																			} else {
																				$price1 = 14;
																			}
																		} else if ($result->b_car_type == 'รถปิคอัพ') {
																			$price1 = 12;
																		} else if ($result->b_car_type == 'รถกระบะทึบ') {
																			$price1 = 15;
																		} else if ($result->b_car_type == 'รถ 6 ล้อ') {

																			$price1 = 20;
																		} else if ($result->b_car_type == 'รถ 10 ล้อ') {

																			$price1 = 30;
																		}

																		echo $price1;
																		?>

																		: <?php
																			if ($result->b_car_type == 'จักรยานยนต์') {
																				if ($m <= 30) {
																					$price2 = 35;
																				} else {
																					$price2 = 35;
																				}
																			} else if ($result->b_car_type == 'รถปิคอัพ') {
																				$price2 = 120;
																			} else if ($result->b_car_type == 'รถกระบะทึบ') {
																				$price2 = 300;
																			} else if ($result->b_car_type == 'รถ 6 ล้อ') {

																				$price2 = 350;
																			} else if ($result->b_car_type == 'รถ 10 ล้อ') {

																				$price2 = 400;
																			}
																			echo $price2;

																			?>
																	</td>
																</tr>
																<tr>
																	<th colspan="3" style="text-align:center;color:red;">รวมราคา</th>
																	<th><?php echo htmlentities($ppdays = $result->b_price); ?> บาท</th>
																</tr>
																<tr>
																	<th>สถานะหนังสือ</th>
																	<td><?php
																		if ($result->b_status == 'รอ') {
																			echo htmlentities('ยังไม่อนุมัติ');
																		} else if ($result->b_status == 1) {
																			echo htmlentities('อนุมัติแล้ว');
																		} else {
																			echo htmlentities('ไม่อนุมัติ');
																		}
																		?></td>
																	<th>วันทำคำร้อง</th>
																	<td><?php echo $result->bb_Regdate; ?></td>
																</tr>

																<tr>
																	<td colspan="2">
																		<img style="width:500px" src="../upload_bill/<?php echo $result->b_img_bill; ?>" alt="">

																	</td>
																	<td colspan="2">
																		<h2>ข้อความ</h2>
																		<br>
																		<div style="background-color: blanchedalmond;"><?php echo $result->message; ?>
																		</div>
																	</td>
																</tr>
																<!-- ************* ปุุ่มอนุมัติ ทำงาน******************* แก้ b_status == 2 -->
																<?php if ($result->b_status == 'รอ') { ?>
																	<tr>
																		<td style="text-align:center" colspan="4">
																			<a href="book_detail.php?aeid=<?php echo htmlentities($result->id); ?>" onclick="return confirm('คุณแน่ใจเหรอ..ว่าจะอนุมัติ..?')" class="btn btn-primary"> อนุมัติหนังสือ</a>

																			<a href="book_detail.php?eid=<?php echo htmlentities($result->id); ?>" onclick="return confirm('คุณแน่ใจเหรอ..ว่าจะปฏิเสธอนุมัติ..?')" class="btn btn-danger"> ปฏิเสธอนุมัติ</a>
																		</td>
																	</tr>

																<?php } ?>
														<?php $cnt = $cnt + 1;
															}
														} ?>

													</tbody>

												</table>



												<form method="post">
													<input name="Submit2" type="submit" class="txtbox4" value="Print" onClick="return f3();" style="cursor: pointer;" />
												</form>

											</div>
										</div>



									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<!-- Loading Scripts -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap-select.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.dataTables.min.js"></script>
		<script src="js/dataTables.bootstrap.min.js"></script>
		<script src="js/Chart.min.js"></script>
		<script src="js/fileinput.js"></script>
		<script src="js/chartData.js"></script>
		<script src="js/main.js"></script>
		<script language="javascript" type="text/javascript">
			function f3() {
				window.print();
			}
		</script>


	</body>

	</html>
<?php } ?>