<?php

session_start();
// include('')
require_once "../connection.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>
    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>

            <div class="showinfo">
                <h1 style=" background-color: yellow">ดูสมาชิก ทั้งหมด</h1>
                <div class="row">
                    <div class="col-8"></div>
                    <div class="col-1">
                        <h4>ค้นหา</h4>
                    </div>

                    <div class="col-2">
                        <input class="input" type="text" id="myInput" onkeyup="myFunction()" placeholder="พิมพ์ค้นหา....">
                    </div>
                </div>
                <?php

                $role = "user";
                $sql = "SELECT * FROM usertbls where role=:role   ";


                $query = $db->prepare($sql);
                $query->bindParam(':role', $role, PDO::PARAM_STR);

                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);


                $cnt = 1;


                if ($query->rowCount() > 0) {
                    foreach ($results as $row) {               ?>


                        <div>


                            <table id="myTable" class="table " style="font-size: 20px;border:double;">


                                <th>
                                    <img style="width:200px ;" src="../upload_person/<?php echo htmlentities($row->img); ?>">
                                </th>

                                <td>

                                    <table>

                                        <tr>
                                            <!-- <th>No.</th> -->
                                            <th>no</th>
                                            <td class="text-center"><?php echo htmlentities($cnt); ?></td>
                                        </tr>
                                        <tr>
                                            <th>username</th>
                                            <td>
                                                <?php echo htmlentities($row->u_name); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ชื่อ นามสกุล</th>
                                            <td>
                                                <?php echo htmlentities($row->name); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เบอร์โทร</th>
                                            <td>
                                                <?php echo htmlentities($row->tel); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>อีเมลล์</th>
                                            <td>
                                                <?php echo htmlentities($row->email); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>วันที่ทำรายการ</th>

                                            <td>
                                                <span class="badge badge-primary"><?php echo htmlentities($row->u_Regdate); ?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="background-color: yellow;">
                                    <div style="margin-top: 70px;" class="form-group">
                                        <a style="color: white;background-color:blue" class="badge  " href="personApp_detail.php?id=<?php echo ($row->id) ?>">ดูรายการสมัครงาน</a>
                                    </div>
                                    <div style="margin-top: 70px;" class="form-group">
                                        <a class="badge  badge-danger " href="personBook_detail.php?u_name=<?php echo ($row->u_name) ?>">ดูรายการจองรถ</a>
                                    </div>
                                </td>


                        <?php $cnt = $cnt + 1;
                    }
                } ?>
                            </table>

                        </div>

            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>

<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- 
<div class="subcontent-area">
        <div class="subcontent-main-div index box-set-width">
            <div class="box with-title is-round">
                
                <div class="box-content">
                    
                    <div class="columns">
                        <div class="column">
                            <p id="totalExpenses" class="font-total-price"></p>                              
                            <p id="totalIncome" class="font-total-price"></p>   
                            <p id="totalProfit" class="font-total-profit"></p>                             
                        </div>
                        <div class="column is-profit-1">
                                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->