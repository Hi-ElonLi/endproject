<?php
session_start();
require_once('connection.php');


if (strlen($_SESSION['user_login']) == 0) {
    header('location:index.php');
}  /// **** กันไม่เข้า login

// ******** เริ่มทำงาน ********
else {

    if (isset($_REQUEST['submit'])) {
        // if (isset($_REQUEST['update_id'])) {
        $message = $_POST['txt_message2'];
        $update_id = $_SESSION['update_id'];
        $bid = $_SESSION['bid'];
        $busy = 'ว่าง';  // รออนุมัต้
        $star = $_POST['txt_star'];

        //** ต่อจาก hire.php... SET busy=:busy  */

        $usq = "UPDATE user_applicate SET busy=:busy WHERE  id=:app_id";
        $update = $db->prepare($usq);
        $update->bindParam(':busy', $busy, PDO::PARAM_STR);
        //** ทำ hire name ด้วยมะ */
        $update->bindParam(':app_id', $bid, PDO::PARAM_STR);   // select from user_book
        $update->execute();

        $idle='เสร็จ';

        $up_stmt = $db->prepare('UPDATE user_book SET message2=:ms, star=:star, idle=:idle  WHERE id=:update_id');
        $up_stmt->bindParam(':update_id', $update_id);
        $up_stmt->bindParam(':star', $star);
        $up_stmt->bindParam(':ms', $message);
        $up_stmt->bindParam(':idle', $idle);
        // $up_stmt->bindParam(':bill', $image_bill);

        if ($up_stmt->execute()) {

            echo "<script>alert('ให้ความเห็น.สำเร็จ...!! ');</script>";

            // $insertMsg = "ดีใจด้วย!!!!!...ทำจองสำเร็จ...";
            header('refresh:1;satisfic.php');
        }
    }


?>
    <!DOCTYPE HTML>
    <html lang="en">

    <head>

        <title>Car Rental Portal - My Booking</title>


        <!--Bootstrap -->
        <link rel="stylesheet" href="resources/css/bootstrap.min2.css" type="text/css">
        <!--Custome Style -->
        <!-- <link rel="stylesheet" href="resources/css/style2.css" type="text/css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .checked {
                color: orange;
            }
        </style>
      
    </head>

    <body style="background-color:lightyellow;">
        <?php include('includes/header.php'); ?>


        <!--Page Header-->
        <section style="  margin-top: 20px;">
            <div class="container" style="  margin-top: px;">
                <h1 style="font-size:50px;background-color:yellow">ผลสำรวจความคิดเห็นการใช้บริการ</h1>
            </div>
        </section>
        <!-- /Page Header-->


        <!-- ****************************from tblusers**********************************************from tblusers************** -->


        <section class="">
            <div class="container " style=" padding: 15px;">

                <div class="row">

                    <div class="col-md-12 col-sm-12" class="form-group">
                        <div class="profile_wrap row">
                            <div class="my_vehicles_list row">
                                <ul class="vehicle_listing">


                                    <!-- ******************  tblbooking ** JOIN ***tblavehical**    **join tblbrands***************  tblbooking *******************************  tblbooking ************** -->

                                    <?php

                                    
                                    $useremail = $_SESSION['user_login'];
                                    // *************************  my sql **********************************
                                    $idle = 'กำลังใช้บริการ';
                                   
                                    //แก้ sql เป็น insert เพราะ มันแยกจองหลายคันได้  + ลบจอง + บิล
                                    $sql2 = "SELECT user_book.*, user_applicate.car_image 
                                    from user_book  join user_applicate on  user_book.bID=user_applicate.id
                                    Where user_book.b_hire_name=:useremail  and idle=:idle   and message2 is null 
                                    ORDER BY id DESC";
                                    // **** เลือกเฉพาะภาพสำหรับ id ที่เราสร้างการจอง
                                    // $sql2 = "SELECT *from user_book 
                                    //          Where user_book.b_hire_name=:useremail   ";
                                    // ******************  tblbooking *************tblbooking.userEmail=:useremail คือ ID******************  tblbooking *******************************  tblbooking ************** -->         


                                    $query2 = $db->prepare($sql2);
                                    $query2->bindParam(':useremail', $useremail, PDO::PARAM_STR);
                                    $query2->bindParam(':idle', $idle, PDO::PARAM_STR);
                                    $query2->execute();
                                    $results_2 = $query2->fetchAll(PDO::FETCH_OBJ);
                                    $cnt = 1;
                                    ?>
                                    <!-- ************************** เริ่ม php ****************** -->
                                    <?php if ($query2->rowCount() > 0) {
                                        foreach ($results_2 as $row) {    ?>
                                            <div class="row">
                                                <!-- //***************************เ ด้านซ้าย********************************************************************* -->
                                                <div class="vehicle_img col-4">

                                                    <h4 style="color:red;font-size:25px;">หมายเลขหนังสือจอง No #<?php echo htmlentities($row->b_BookingID); ?></h4>
                                                    <img width="350" height="300" class="img-responsive" alt="Image" src="upload_car/<?php echo htmlentities($row->car_image); ?>" alt="image">

                                                </div>
                                                <!-- //************************ แก้  vด้านกลาง ************ -->
                                                <div class="vehicle_title col-4 " style="font-size: 50px;">

                                                    <h6 style="margin-bottom:25px;font-size: 30px;">ประเภทรถ : <?php echo htmlentities($row->b_car_type); ?> </h6>
                                                    <h6 style="margin-bottom:25px;font-size: 30px;">ชื่อผู้ขับ : <?php echo htmlentities($row->b_driver_name); ?> </h6>

                                                    <p style="margin-bottom:25px;font-size: 30px;"><b>วันจอง </b> <?php echo htmlentities($row->FromDate); ?></p>
                                                    <div style="float: left; ">
                                                        <p style="font-size: 30px; background-color:lightyellow"><b>Message:</b> <br> 
                                                            <?php echo htmlentities($row->message); ?> </p>
                                                    </div>

                                                </div>

                                                <!-- *********** if else พัง หรือ เพราะ ui พัง(โชวที่กรอกตารางอื่น)  -->


                                                <?php if ($row->message2 == "") { ?>

                                                    <!-- //** ด้านขวา */ -->
                                                    <div class=" vehicle_status col-4">
                                                        <form method="post" enctype="multipart/form-data">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div></div>
                                                                    <h4 style="color:blue">ให้คะแนน</h4>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                            </td>
                                                                            <td>
                                                                                5 คะแนน
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star"></span>
                                                                            </td>
                                                                            <td>
                                                                                4 คะแนน
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <span class="fa fa-star"></span>
                                                                            </td>
                                                                            <td>
                                                                                3 คะแนน
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star "></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <span class="fa fa-star"></span>
                                                                            </td>
                                                                            <td>
                                                                                2 คะแนน
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star "></span>
                                                                                <span class="fa fa-star "></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <span class="fa fa-star"></span>
                                                                            </td>
                                                                            <td>
                                                                                1 คะแนน
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <div class="">
                                                                        <select style="font-size:15px;" name="txt_star" id="" class="">
                                                                            <option value="" selected="selected">- เลือกคะแนน -</option>
                                                                            <option value="5">5 ดาว</option>
                                                                            <option value="4">4 ดาว</option>
                                                                            <option value="3">3 ดาว</option>
                                                                            <option value="2">2 ดาว</option>
                                                                            <option value="1">1 ดาว</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <label style="font-size:15px;">ข้อความ:</label>
                                                                <div class="">
                                                                    <textarea style="font-size:15px" rows="4" class="form-control" name="txt_message2" placeholder="Message" required></textarea>
                                                                </div>
                                                                <!-- //********************* ปุ่มส่ง */                -->
                                                                <?php
                                                                $_SESSION['update_id'] = $row->id;
                                                                $_SESSION['bid'] = $row->bID;
                                                                ?>

                                                                <div>

                                                                    <input type="submit" name="submit" value="ส่งความเห็น" class="btn11" required>

                                                                    <!-- ///** input ต้องมี tag submit */ -->
                                                                </div>
                                                            </div>


                                                        </form>
                                                    </div>

                                                <?php } else { ?>

                                                    <div class=" vehicle_status">
                                                        <a href="#" style="margin:10px; background-color:green;" class="btn" style="margin:10px;">ส่งข้อความแล้ว</a>
                                                    </div>

                                                <?php      } ?>

                                            
                                            </div> 
                                            <hr style="border:solid" />


                                            <!-- *************ถ้าเกิดไม่มีข้อมูลอ****************** -->





                                        <?php   }
                                    } else { ?>
                                        <h1 align="center" style="color:red">ยังไม่มีรายอนุมัติ</h1>
                                        <?php   } ?>


                                </ul>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
            </div>
        </section>

    </body>



    <style>
        .btn0 {
            border: medium none;
            border-radius: 3px;

            /* display: inline-block; */

        }

        .btn2 {
            border: medium none;
            border-radius: 3px;
            color: white;
            background-color: red;
            font-size: 16px;
            font-weight: 800;
            line-height: 30px;
            width: 200px;
            margin: auto;
            padding: 7px 36px;

            /* the default for span */

        }

        .btn1 {
            border: medium none;
            border-radius: 3px;
            color: white;
            background-color: green;
            font-size: 16px;
            font-weight: 800;
            line-height: 30px;
            width: 200px;
            margin: auto;
            padding: 7px 36px;

            /* display: inline-block; */

        }

        table {
            margin: 0 0 40px;
            width: 100%;

        }

        table th,
        table td {
            /* border: 1px double#cccccc; */
            padding-left: 15px;
            padding-right: 15px;
            /* padding: 18px; */
        }

        /*
        
        table th img,
        table td img {
            max-width: 100%;
        }

        table thead {
            background: #eee;
        }

        table thead th,
        table thead td {
            text-transform: uppercase;
            font-weight: 900;
            color: #111;
        } */


        table,
        th,
        td,
        tr {
            /* align-self: right; */
            /* width:max-content; */
            border: solid 1px;
            font-size: 15px;

        }

        .btn11 {
            background-color: green;
            border: medium none;
            border-radius: 3px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 800;
            line-height: 30px;
            margin: auto;
            padding: 7px 36px;
            transition: all 0.3s linear 0s;
            -moz-transition: all 0.3s linear 0s;
            -o-transition: all 0.3s linear 0s;
            -webkit-transition: all 0.3s linear 0s;
            -ms-transition: all 0.3s linear 0s;
        }

        /* container {
            height: 200px;
            position: relative;
            border: 3px solid green;
        } */

        /* .center { */
        /* margin: 0; */
        /* position: absolute; */
        /* top: 50%; */
        /* left: 50%; */
        /* -ms-transform: translate(-50%, -50%); */
        /* transform: translate(-50%, -50%); */
        /* } */
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </html>




<?php } ?>